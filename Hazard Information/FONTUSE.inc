;--------------------------------------------------------------------------------
;			FONT USE DETAILS
;--------------------------------------------------------------------------------
; Part of HAZARD Library
; (a game/software programming library for QuickBasic 4.5 or similar)
; 
; Version: first
; by WolfRAM
;********************************************************************************
;
;
;
;
;
; Font Size	XSixe x YSize
;	This is to be stored in CONV variables.
;	Separately for Font1 and Font2.
;
; FONT 01
;	EMS page	-	2
;	EMS offset	-	0
;
; FONT 02
;	EMS page	-	3
;	EMS offset	-	0
;
; Max. permitted size for each font character	-	256 bytes
;
;
; Larger fonts be be used by direct from file method.
; Font storage will be bytewise, not bitwise.
;
;
;