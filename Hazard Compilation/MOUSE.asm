;--------------------------------------------------------------------------------
;		MOUSE MACHINE
;--------------------------------------------------------------------------------
; Part of HAZARD Library
; (a game/software programming library for QuickBasic 4.5 or similar)
; 
; Version: first
; by WolfRAM
;********************************************************************************





.MODEL Large, Basic

.386

INCLUDE	Includes.inc


;SHARED



.STACK 100h

EXTRN	LastError:WORD



;CONST






.DATA







;External SUBs



.CODE








; -----------------------------------------------------------------------------------------------------------------------------
;		MACROS
; -----------------------------------------------------------------------------------------------------------------------------
IsBoundaryWithin	MACRO	x, y, notwithin
mov	ax, x
cmp	ax, BoundaryX1
jb	notwithin
cmp	ax, BoundaryX2
ja	notwithin
mov	ax, y
cmp	ax, BoundaryY1
jb	notwithin
cmp	ax, BoundaryY2
ja	notwithin
ENDM




RearrangeXY		MACRO	x1, y1, x2, y2
mov	ax, x1
cmp	ax, x2
jbe	x1lex2
xchg	ax, x2
mov	x1, ax

x1lex2:
mov	ax, y1
cmp	ax, y2
jbe	y1ley2
xchg	ax, y2
mov	y1, ax

y1ley2:
ENDM



BringWithinDefaultValue	MACRO	x, lowrange, highrange, xok1, xok2
cmp	WORD PTR x, lowrange
jge	xok1
mov	WORD PTR x, lowrange

xok1:
cmp	WORD PTR x, highrange
jle	xok2
mov	WORD PTR x, highrange

xok2:
ENDM



BringWithinDefault	MACRO	x1, y1, x2, y2
BringWithinDefaultValue	x1, 0, 319, a1, b1
BringWithinDefaultValue	x2, 0, 319, a2, b2
BringWithinDefaultValue	y1, 0, 199, a3, b3
BringWithinDefaultValue	y2, 0, 199, a4, b4
ENDM



GetPixelAddress		MACRO	x, y
xor	ax, ax
xor	bx, bx
mov	ah, y
mov	bh, ah
shr	ax, 2
add	bx, ax
add	bx, x
ENDM





BringWithinBoundaryX	MACRO	x, xok1, xok2
mov	ax, x
cmp	ax, BoundaryX1
jae	xok1
mov	ax, BoundaryX1

xok1:
cmp	ax, BoundaryX2
jbe	xok2
mov	ax, BoundaryX2

xok2:
mov	x, ax
ENDM




BringWithinBoundaryY	MACRO	y, yok1, yok2
mov	ax, y
cmp	ax, BoundaryY1
jae	yok1
mov	ax, BoundaryY1

yok1:
cmp	ax, BoundaryY2
jbe	yok2
mov	ax, BoundaryY2

yok2:
mov	y, ax
ENDM




BringWithinBoundary	MACRO	x1, y1, x2, y2
BringWithinBoundaryX	x1
BringWithinBoundaryY	y1
BringWithinBoundaryX	x2
BringWithinBoundaryY	y2
ENDM



GetPixelAddress		MACRO	x, y
push	ax
xor	ax, ax
xor	bx, bx
mov	ah, y
mov	bh, ah
shr	ax, 2
add	bx, ax
add	bx, x
pop	ax
ENDM













; -----------------------------------------------------------------------------------------------------------------------------
;		INTERNAL FUNCTIONS
; -----------------------------------------------------------------------------------------------------------------------------



; -----------------------------------------------------------------------------------------------------------------------------
; StartMouse			INTERNAL FUNCTION
; Purpose:
;   Starts the mouse machine
;
; Usage:
;   none
;
; Returns:
;   nothing
; -----------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC StartMouse
StartMouse PROC
push4	ax, cx, dx, es
xor	ax, ax
int	33h
or	ax, ax
jns	mouseerr
mov	ax, 7h
mov	cx, defboundaryx1
mov	dx, defboundaryx2
int	33h
mov	ax, 8h
mov	cx, defboundaryy1
mov	dx, defboundaryy2
int	33h
mov	ax, SEG MouseISR
mov	es, ax
mov	dx, OFFSET MouseISR
mov	cx, 11111b
mov	ax, 0Ch
int	33h
pop4	ax, cx, dx, es
retf

MouseISR:
iret
StartMouse ENDP




; -----------------------------------------------------------------------------------------------------------------------------
; StopMouse			INTERNAL FUNCTION
; Purpose:
;   Stops the mouse machine
;
; Usage:
;   none
;
; Returns:
;   nothing
; -----------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC StopMouse
StopMouse PROC
push	ax
xor	ax, ax
int	33h
pop	ax
retf
StopKeyboard ENDP










; -----------------------------------------------------------------------------------------------------------------------------
;		PUBLIC FUNCTIONS
; -----------------------------------------------------------------------------------------------------------------------------





; -----------------------------------------------------------------------------------------------------------------------------
; HZDmouseDisplay		SUB
;
; Purpose:
;   Turn mouse display on or off
;
; Declaration:
;   DECLARE SUB HZDmouseDisplay(BYVAL drw%)
;
; Returns:
;   nothing
; -----------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC HZDmouseDisplay
HZDmouseDisplay PROC
UseParam
cmp	VideoActive, 1
je	graphicsmodeactive
mov	ax, param1
not	ax
and	ax, 1
inc	ax
int	33h

over98:
EndParam
retf	2

graphicsmodeactive:
mov	al, param1
and	al, 1
mov	MouseDisplay, al
jmp	over98
HZDmouseDisplay ENDP





; -----------------------------------------------------------------------------------------------------------------------------
; HZDsetMouseRange	SUB
;
; Purpose:
;   Set the range in which mouse can move
;
; Declaration:
;   DECLARE SUB HZDsetMouseRange(BYVAL x1%, BYVAL y1%, BYVAL x2%, BYVAL y2%)
;
; Returns:
;   nothing
; -----------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC HZDsetMouseRange
HZDsetMouseRange PROC
UseParam
RearrangeXY	param4, param3, param2, param1
BringWithinDefault	param4, param3, param2, param1
mov	cx, param4
mov	MsBoundaryX1, cx
mov	dx, param2
mov	MsBoundaryX2, dx
mov	ax, 7
int	33h
mov	cx, param3
mov	MsBoundaryY1, cx
mov	dx, param1
mov	MsBoundaryY2, dx
mov	ax, 8
int	33h
EndParam
retf	8
HZDsetMouseRange ENDP







; -----------------------------------------------------------------------------------------------------------------------------
; HZDsetMousePosition	SUB
;
; Purpose:
;   Set the position of the mouse
;
; Declaration:
;   DECLARE SUB HZDsetMousePosition(BYVAL x%, BYVAL y%)
;
; Returns:
;   nothing
; -----------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC HZDsetMousePosition
HZDsetMousePosition PROC
UseParam
BringWithinBoundaryX	param2, bla01, bla02
BringWithinBoundaryY	param1, bla03, bla04
mov	cx, param2
mov	dx, param1
mov	ax, 4
int	33h
EndParam
retf	4
HZDsetMousePosition ENDP




; -----------------------------------------------------------------------------------------------------------------------------
; HZDsetMouseSensitivity	SUB
;
; Purpose:
;   Set the sensitivity of the mouse
;
; Declaration:
;   DECLARE FUNCTION HZDsetMouseSensitivity(BYVAL sensex%, BYVAL sensey%)
;
; Returns:
;   nothing
; -----------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC HZDsetMouseSensitivity
HZDsetMouseSensitivity PROC
UseParam
mov	cx, param2
mov	dx, param1
mov	ax, 0Fh
int	33h
EndParam
retf	4
HZDsetMouseSensitivity ENDP




; -----------------------------------------------------------------------------------------------------------------------------
; HZDsetMouseDoubleSpeed	SUB
;
; Purpose:
;   Set the double speed threshold speed of the mouse.
;   It is the speed of the mouse when speed of cursor is doubled.
;
; Declaration:
;   DECLARE FUNCTION HZDsetMouseDoubleSpeed(BYVAL dblspd%)
;
; Returns:
;   nothing
; -----------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC HZDsetMouseDoubleSpeed
HZDsetMouseDoubleSpeed PROC
UseParam
mov	dx, param1
mov	ax, 13h
int	33h
EndParam
retf	2
HZDsetMouseDoubleSpeed ENDP






; -----------------------------------------------------------------------------------------------------------------------------
; HZDgetMouseStatus	SUB
;
; Purpose:
;   Gives the current mouse status(position of mouse and state of buttons)
;
; Declaration:
;   DECLARE SUB HZDgetMouseStatus(status1 AS HZDmouseStatus)
;
; Returns:
;   status1.X=current column of mouse on the screen
;   status1.Y=current row of mouse on the screen
;   status1.Button1=1 if left mouse button is pressed, else 0
;   status1.Button2=1 if right mouse button is pressed, else 0
; -----------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC HZDgetMouseStatus
HZDgetMouseStatus PROC
UseParam
push	si
mov	si, param1
mov	ax, 3
int	33h
mov	[si], cx
mov	[si+2], dx
mov	cx, bx
and	cx, 1
mov	[si+4], cx
mov	cx, bx
shr	cx, 1
mov	[si+6], cx
pop	si
EndParam
retf	2
HZDgetMouseStatus ENDP



; -----------------------------------------------------------------------------------------------------------------------------
; HZDgetDelMouseStatus SUB
;
; Purpose:
;   Gives the change in mouse status(change in position of mouse and state of buttons)
;   
; Declaration:
;   DECLARE SUB HZDgetDelMouseStatus(status1 AS HZDmouseStatus)
;
; Returns:
;   status1.X=change in column of mouse since last call(no border)
;   status1.Y=change in row of mouse since last call(no border)
;   status1.Button1=number of changes in left mouse button state
;   status1.Button2=number of changes in right mouse button state
; -----------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC HZDgetDelMouseStatus
HZDgetDelMouseStatus PROC
UseParam
push	si
mov	si, param1
mov	ax, 0Bh
int	33h
mov	[si], cx
mov	[si+2], dx
mov	cx, LeftButtonDelta
mov	[si+4], cx
mov	cx, RightButtonDelta
mov	[si+6], cx
pop	si
EndParam
retf	2
HZDgetDelMouseStatus ENDP




; -----------------------------------------------------------------------------------------------------------------------------
; HZDgetMouseButtonUseStatus SUB
;
; Purpose:
;   Gives the information related to button press
;   
; Declaration:
;   DECLARE SUB HZDgetMouseButtonUseStatus(status1 AS HZDmouseStatus, BYVAL buttonuse%, BYVAL button%)
;
; Returns:
;   status1.X=column of mouse where the specified button was pressed/released
;   status1.Y=row of mouse where the specified button was pressed/released
;   status1.Button1=number of times the specified button was pressed/released
; -----------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC HZDgetMouseButtonUseStatus
HZDgetMouseButtonUseStatus PROC
UseParam
push	si
mov	si, param3
mov	bx, param1
mov	ax, param2
and	ax, 1
add	ax, 5
int	33h
mov	[si], cx
mov	[si+2], dx
mov	[si+4], bx
pop	si
EndParam
retf	4
HZDgetMouseButtonUseStatus ENDP









;Make Button
;Get Button Status














END