;--------------------------------------------------------------------------------
;			IMAGE MACHINE
;--------------------------------------------------------------------------------
; Part of HAZARD Library
; (a game/software programming library for QuickBasic 4.5 or similar)
; 
; Version: first
; by WolfRAM
;********************************************************************************







.MODEL Large, Basic

.386

INCLUDE	Includes.inc


;SHARED





.STACK 200h

EXTRN	LastError:WORD




;CONST
verticalretraceport			equ	3DAh




.DATA
ImageProcessing			DW	OFFSET imgprcsmode4, SEG imgprcsmode4, OFFSET imgprcsmode5, SEG imgprcsmode5
				DW	OFFSET imgprcsmode6, SEG imgprcsmode6, OFFSET imgprcsmode7, SEG imgprcsmode7



;External SUBS




.CODE







; -----------------------------------------------------------------------------------------------------------------------------
;		MACROS
; -----------------------------------------------------------------------------------------------------------------------------
IsBoundaryWithin	MACRO	x, y, notwithin
mov	ax, x
cmp	ax, BoundaryX1
jb	notwithin
cmp	ax, BoundaryX2
ja	notwithin
mov	ax, y
cmp	ax, BoundaryY1
jb	notwithin
cmp	ax, BoundaryY2
ja	notwithin
ENDM




RearrangeXY		MACRO	x1, y1, x2, y2
mov	ax, x1
cmp	ax, x2
jbe	x1lex2
xchg	ax, x2
mov	x1, ax

x1lex2:
mov	ax, y1
cmp	ax, y2
jbe	y1ley2
xchg	ax, y2
mov	y1, ax

y1ley2:
ENDM






BringWithinDefault	MACRO	x1, y1, x2, y2
cmp	x1, 320
jbe	x1ok
mov	x1, 320

x1ok:
cmp	x2, 320
jbe	x2ok
mov	x2, 320

x2ok:
cmp	y1, 200
jbe	y1ok
mov	y1, 200

y1ok:
cmp	y2, 200
jbe	y2ok
mov	y2, 200

y2ok:
ENDM



GetPixelAddress		MACRO	x, y
xor	ax, ax
xor	bx, bx
mov	ah, y
mov	bh, ah
shr	ax, 2
add	bx, ax
add	bx, x
ENDM





BringWithinBoundaryX	MACRO	x
mov	ax, x
cmp	ax, BoundaryX1
jae	xok1
mov	ax, BoundaryX1

xok1:
cmp	ax, BoundaryX2
jbe	xok2
mov	ax, BoundaryX2

xok2:
mov	x, ax
ENDM




BringWithinBoundaryY	MACRO	y
mov	ax, y
cmp	ax, BoundaryY1
jae	yok1
mov	ax, BoundaryY1

yok1:
cmp	ax, BoundaryY2
jbe	yok2
mov	ax, BoundaryY2

yok2:
mov	y, ax
ENDM




BringWithinBoundary	MACRO	x1, y1, x2, y2
BringWithinBoundaryX	x1
BringWithinBoundaryY	y1
BringWithinBoundaryX	x2
BringWithinBoundaryY	y2
ENDM



GetPixelAddress		MACRO	x, y
push	ax
xor	ax, ax
xor	bx, bx
mov	ah, y
mov	bh, ah
shr	ax, 2
add	bx, ax
add	bx, x
pop	ax
ENDM















; -----------------------------------------------------------------------------------------------------------------------------
;		INTERNAL FUNCTIONS
; -----------------------------------------------------------------------------------------------------------------------------



; -----------------------------------------------------------------------------------------------------------------------------
; StartImage	INTERNAL FUNCTION
;
; Purpose:
;   Starts the image machine(load general PICs)
;
; Usage:
;   none
;
; Returns:
;   nothing
; -----------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC StartImage
StartImage PROC
push	ax
mov	ax, 13h
int	10h
pop	ax
retf
StartImage ENDP


; -----------------------------------------------------------------------------------------------------------------------------
; StopImage	INTERNAL FUNCTION
;
; Purpose:
;   Stops the graphics machine
;
; Returns:
;   nothing
; -----------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC StopImage
StopImage PROC
push	ax
mov	ax, 3h
int	10h
pop	ax
mov	BoundaryX1, 0
mov	BoundaryY1, 0
mov	BoundaryX2, 320
mov	BoundaryY2, 200
mov	ax, VideoSEG
mov	GraphicsPage, ax
retf
StopImage ENDP




;   0=normal, 1=clipped, 2=colour normal, 3=colour clipped, 4=add normal
;   5=flipped, 6=rotated, 7=sized
;   ah:fs:si=image address, bh=perform on image, bl=image mode, cx=control value
;   0=normal, 1=clipped, 2=colour normal, 3=colour clipped, 4=add normal
;   5=flipped, 6=rotated, 7=sized

; -----------------------------------------------------------------------------------------------------------------------------
; ProcessImage	INTERNAL FUNCTION
;
; Purpose:
;   Processes an Image from memory
;
; Usage:
;   ah:fs:si=image address, bl=image mode, cx=control value
;   4=add normal, 5=flipped, 6=rotated, 7=sized
;
; Returns:
;   nothing
; -----------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC ProcessImage
ProcessImage PROC
push7	esi, es, edi, ax, bx, ecx, dx
push	bp
sub	sp, xxxx
mov	stack0, ax
mov	stack2, fs
mov	stack4, ebx
mov	stack8, cx
mov	bp, sp
mov	cl, ah
mov	ax, fs
mov	ebx, esi
call	GetMem
mov	fs, ax
mov	ebx, stack4
sub	bl, 4
xor	bh, bh
shl	bx, 2
jmp	ImageProcessing[bx]

over00:
add	sp, xxxx
pop	bp
pop7	esi, es, edi, ax, bx, ecx, dx
retf


; Image Processing Mode 4
; Add Normal Mode
; cx=value to add to each byte
imgprcsmode4:
push8	ds, si, es, di, ax, bx, cx, dx
mov	ax, fs
mov	ds, ax
mov	es, ax
mov	di, si
mov	cx, [si+4]
sub	cx, 4
add	si, 12
mov	bx, stack8
mov	dx, cx
shr	cx, 1
cld

loopdo01:
lodsw
add	al, bl
add	ah, bl
stosw
dec	cx
jnz	loopdo01
mov	cx, dx
and	cx, 1
jz	okay01
lodsb
add	al, bl
stosb

okay01:
pop8	ds, si, es, di, ax, bx, cx, dx
jmp	over00


imgprcsmode5:

; Image Processing Mode 5
; Flipped image Mode
; cx=0-XFlip, 1-YFlip

ProcessImage ENDP









; -----------------------------------------------------------------------------------------------------------------------------
;		PUBLIC FUNCTIONS
; -----------------------------------------------------------------------------------------------------------------------------


; -----------------------------------------------------------------------------------------------------------------------------
; HZDselectGraphicsPage	SUB
;
; Purpose:
;   Select a graphics page from the four 
;   available graphics pages(0-3)
;
; Declaration:
;   DECLARE SUB HZDselectGraphicsPage(BYVAL page%)
;
; Returns:
;   nothing
; -----------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC HZDselectGraphicsPage
HZDselectGraphicsPage PROC
UseParam
mov	ax, param1
and	ax, 3
add	ax, VideoSEG
mov	GraphicsPage, ax
EndParam
retf	2
HZDselectGraphicsPage ENDP











END
