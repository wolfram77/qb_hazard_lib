;--------------------------------------------------------------------------------
;			FONT MACHINE
;--------------------------------------------------------------------------------
; Part of HAZARD Library
; (a game/software programming library for QuickBasic 4.5 or similar)
; 
; Version: first
; by WolfRAM
;********************************************************************************







.MODEL Large, Basic

.386

INCLUDE	Includes.inc


;SHARED





.STACK 200h

EXTRN	LastError:WORD
EXTRN	VideoTask:BYTE





;CONST
fontOFF		equ	0




.DATA
FontSEG		DW	2
FontClipping	DB	0
FontGapping	DW	0
FontColour	DB	1




;External SUBS
EXTRN	CopyMemEE:FAR
EXTRN	GetMemE:FAR
EXTRN	CopyMem:FAR



.CODE

; -----------------------------------------------------------------------------------------------------------------------------
;		INTERNAL FUNCTIONS
; -----------------------------------------------------------------------------------------------------------------------------



; -----------------------------------------------------------------------------------------------------------------------------
; StartFont	INTERNAL FUNCTION
;
; Purpose:
;   Start the font machine.
;
; Returns:
;   nothing
; -----------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC StartFont
StartFont PROC
push5	es, di, ax, bx, dx
mov	FontSEG, 2
mov	fs, LibReadHandle
mov	si, deffontADRS
mov	es, FontSEG
mov	di, fontOFF
mov	ecx, deffonttablesize
call	
StartFont ENDP


; -----------------------------------------------------------------------------------------------------------------------------
; ActivatePalette	INTERNAL FUNCTION
;
; Purpose:
;   Activates the current palette during vertical retrace period.
;
; Returns:
;   nothing
; -----------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC ActivatePalette
ActivatePalette PROC
push5	ds, si, ax, bx, dx
mov	ax, PaletteSEG
call	GetMemE
mov	ds, ax
mov	si, nowpaletteOFF
xor	bx, bx
cld

actclrs:
mov	dx, setpaletteport
mov	al, bl
out	dx, al
inc	dx
lodsw
out	dx, al
mov	al, ah
out	dx, al
lodsb
out	dx, al
inc	bx
cmp	bx, palettecolours
jb	actclrs
pop5	ds, si, ax, bx, dx
retf
ActivatePalette	ENDP



; -----------------------------------------------------------------------------------------------------------------------------
; CreateNowPalette	INTERNAL FUNCTION
;
; Purpose:
;   Create the palette to be activated from the set palette.
;
; Returns:
;   nothing
; -----------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC CreateNowPalette
CreateNowPalette PROC
push6	ds, si, es, di, ax, bx
push	cx
mov	ax, PaletteSEG
call	GetMemE
mov	ds, ax
mov	si, usrpaletteOFF
mov	es, ax
mov	di, nowpaletteOFF
xor	ax, ax
mov	cx, palettesize

createnowpal:
lodsb
sub	al, 32
mov	bl, Contrast
imul	bl
shr	ax, 5
add	ax, 32
add	al, Brightness
cmp	ax, 0
jge	clrok1
mov	ax, 0

clrok1:
cmp	ax, 63
jle	clrok2
mov	al, 63

clrok2:
stosb
dec	cx
jnz	createnowpal
or	VideoTask, 2
pop	cx
pop6	ds, si, es, di, ax, bx
retf
CreateNowPalette ENDP







; -----------------------------------------------------------------------------------------------------------------------------
; SetPalette	INTERNAL FUNCTION
;
; Purpose:
;   Set a new palette.
;
; Usage:
;   ah=type, fs:si=address of new palette
;
; Returns:
;   nothing
; -----------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC SetPalette
SetPalette PROC
push4	ax, es, di, ecx
mov	al, 1
mov	es, PaletteSEG
mov	di, usrpaletteOFF
mov	ecx, palettesize
call	CopyMem
call	CreateNowPalette
pop4	ax, es, di, ecx
retf
SetPalette	ENDP



; -----------------------------------------------------------------------------------------------------------------------------
;		PUBLIC FUNCTIONS
; -----------------------------------------------------------------------------------------------------------------------------


; -----------------------------------------------------------------------------------------------------------------------------
; HZDsetFontClip	SUB
;
; Purpose:
;   Set wether font will be transparent or opaque.
;
; Declaration:
;   DECLARE SUB HZDsetFontClip(BYVAL onoff%)
;
; Returns:
;   nothing
; -----------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC HZDsetFontClip
HZDsetFontClip PROC
UseParam
mov	al, param1
and	al, 1
mov	FontClipping, al
EndParam
retf	2
HZDsetFontClip ENDP



; -----------------------------------------------------------------------------------------------------------------------------
; HZDsetFontColour	SUB
;
; Purpose:
;   Set the colour of the font.
;
; Declaration:
;   DECLARE SUB HZDsetFontColour(BYVAL colour%)
;
; Returns:
;   nothing
; -----------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC HZDsetFontColour
HZDsetFontColour PROC
UseParam
mov	al, param1
mov	FontColour, al
EndParam
retf	2
HZDsetFontColour ENDP



; -----------------------------------------------------------------------------------------------------------------------------
; HZDsetDefFont	SUB
;
; Purpose:
;   Set the default font.
;
; Declaration:
;   DECLARE SUB HZDsetDefFont()
;
; Returns:
;   nothing
; -----------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC HZDsetDefFont
HZDsetDefFont PROC
mov	fs, LibFileHandle
mov	esi, deffontADRS
mov	es, FontSEG
mov	di, fontOFF
mov	ecx, deffontSIZE
call	CopyMemFE

mov	ax, param1
cmp	ax, 0
jge	brlowok
mov	ax, 0

brlowok:
cmp	ax, 63
jle	brhighok
mov	ax, 63

brhighok:
sub	ax, 32
mov	Brightness, al
call	CreateNowPalette
retf
HZDsetDefFont ENDP



; -----------------------------------------------------------------------------------------------------------------------------
; HZDsetContrast	SUB
;
; Purpose:
;   Set contrast.
;
; Declaration:
;   DECLARE SUB HZDsetContrast(BYVAL contrast%)
;
; Returns:
;   nothing
; -----------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC HZDsetContrast
HZDsetContrast PROC
UseParam
mov	ax, param1
cmp	ax, 0
jge	brlowok
mov	ax, 0

brlowok:
cmp	ax, 63
jle	brhighok
mov	ax, 63

brhighok:
mov	Contrast, al
call	CreateNowPalette
EndParam
retf	2
HZDsetContrast ENDP




END
