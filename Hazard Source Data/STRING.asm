;--------------------------------------------------------------------------------
;			STRING MACHINE
;--------------------------------------------------------------------------------
; Part of HAZARD Library
; (a game/software programming library for QuickBasic 4.5 or similar)
; 
; Version: first
; by WolfRAM
;********************************************************************************







.MODEL Large, Basic

.386

INCLUDE	Includes.inc


;SHARED





.STACK 200h



;CONST




.DATA






;External SUBS





.CODE

; -----------------------------------------------------------------------------------------------------------------------------
;		INTERNAL FUNCTIONS
; -----------------------------------------------------------------------------------------------------------------------------



; -----------------------------------------------------------------------------------------------------------------------------
; CompareStringsCC		INTERNAL FUNCTION
;
; Purpose:
;   Compare two strings(both in CONV)
;
; Usage:
;   fs:si=string1, es:di=string2
;
; Returns:
;   ax=0 if equal, ax=-1 if not equal
; -----------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC CompareStringsCC
CompareStringsCC PROC
push5	ds, si, di, cx, dx
mov	ax, fs
mov	ds, ax
mov	dx, fs:[si]
cmp	dx, es:[di]
jne	notequal
mov	cx, dx
shr	cx, 2
repe	cmpsd
jne	notequal
mov	cx, dx
and	cx, 3
repe	cmpsb
jne	notequal
xor	ax, ax

over0:
pop5	ds, si, di, cx, dx
retf

notequal:
mov	ax, -1
jmp	over0
CompareStringsCC ENDP





; -----------------------------------------------------------------------------------------------------------------------------
; AscizToLasciz		INTERNAL FUNCTION
;
; Purpose:
;   Convert an ASCIZ string to LASCIZ string
;
; Usage:
;   fs:si=string address in CONV(must be >=2)
;
; Returns:
;   fs:si=converted string(si=si-2)
; -----------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC AscizToLasciz
AscizToLasciz PROC
push2	ax, cx
push2	ds, si
mov	ax, fs
mov	ds, ax
xor	cx, cx
cld

readnext01:
lodsb
or	al, al
jz	over66
inc	cx
jmp	readnext01

over66:
pop2	ds, si
sub	si, 2
mov	[si], cx
pop2	ax, cx
retf
AscizToLasciz ENDP





; -----------------------------------------------------------------------------------------------------------------------------
;		PUBLIC FUNCTIONS
; -----------------------------------------------------------------------------------------------------------------------------





; -----------------------------------------------------------------------------------------------------------------------------
; HZDcompareStrings	FUNCTION
;
; Purpose:
;   Compare two strings(both in CONV)
;
; Declaration:
;   DECLARE FUNCTION HZDcompareStrings%(BYVAL srcSEG%, BYVAL srcOFF%,
;				        BYVAL desSEG%, BYVAL desOFF%)
;
; Returns:
;   true if equal, false if not equal
; -----------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC HZDcompareStrings
HZDcompareStrings PROC
UseParam
push4	fs, si, es, di
mov	fs, param4
mov	si, param3
mov	es, param2
mov	di, param1
call	CompareStringsCC
not	ax
pop4	fs, si, es, di
EndParam
retf	8
HZDcompareStrings ENDP





END
