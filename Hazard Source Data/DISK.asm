;--------------------------------------------------------------------------------
;			DISK MACHINE
;--------------------------------------------------------------------------------
; Part of HAZARD Library
; (a game/software programming library for QuickBasic 4.5 or similar)
; 
; Version: first
; by WolfRAM
;********************************************************************************







.MODEL Large, Basic

.386

INCLUDE	Includes.inc


;SHARED





.STACK 200h

EXTRN	LastError:WORD




;CONST




.DATA



;External SUBS




.CODE












; -----------------------------------------------------------------------------------------------------------------------------
;		INTERNAL FUNCTIONS
; -----------------------------------------------------------------------------------------------------------------------------







; -----------------------------------------------------------------------------------------------------------------------------
; SetRefreshRate	INTERNAL FUNCTION
;
; Purpose:
;   Set the refresh rate
;
; Usage:
;   al=refresh rate(in Hz)
;
; Returns:
;   nothing
; -----------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC SetRefreshRate
SetRefreshRate PROC








; -----------------------------------------------------------------------------------------------------------------------------
;		PUBLIC FUNCTIONS
; -----------------------------------------------------------------------------------------------------------------------------


; -----------------------------------------------------------------------------------------------------------------------------
; HZDnumFixedDrives	FUNCTION
;
; Purpose:
;   Gives the number of fixed drives(C:,D:,etc.)
;
; Declaration:
;   DECLARE FUNCTION HZDnumFixedDrives()
;
; Returns:
;   number of fixed drives
; -----------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC HZDnumFixedDrives
HZDnumFixedDrives PROC
mov	ax, 40h
mov	es, ax
mov	al, es:[75h]
xor	ah, ah
retf
HZDnumFixedDrives ENDP





; -----------------------------------------------------------------------------------------------------------------------------
; HZDcurrentDriveName	SUB
;
; Purpose:
;   Gives the name of current drives(C:/D:/E:/,etc.)
;
; Declaration:
;   DECLARE SUB HZDcurrentDriveName(driveletter AS HZDdrive)
;
; Returns:
;   current drive name
; -----------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC HZDcurrentDriveName
HZDcurrentDriveName PROC
UseParam
mov	ah, 19h
int	21h
add	al, 65
mov	ah, ':'
mov	bx, param1
mov	[bx], ax
EndParam
retf	2
HZDcurrentDriveName ENDP





; -----------------------------------------------------------------------------------------------------------------------------
; HZDcurrentDirectory	FUNCTION
;
; Purpose:
;   Gives the current directory(address)
;
; Declaration:
;   DECLARE FUNCTION HZDcurrentDirectory$()
;
; Returns:
;   current drive name
; -----------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC HZDcurrentDirectory
HZDcurrentDirectory PROC
mov	ax, RoughSEG[2]
call	GetMemE
push	ds
mov	ds, ax
mov	si, roughOFF+2
xor	dl, dl
mov	ah, 47h
int	21h
pop	ds
mov	fs, EMSseg
call	AscizToLasciz
call	StringToQBrough
retf
HZDcurrentDirectory ENDP









; -----------------------------------------------------------------------------------------------------------------------------
; HZDfindFirst	SUB
;
; Purpose:
;   Start a search for files(and directories) in the current directory
;
; Declaration:
;   DECLARE SUB HZDfindFirst(search1 AS HZDfindDirectory, FileName$, BYVAL includes1%)
;
; Returns:
;   current drive name
; -----------------------------------------------------------------------------------------------------------------------------
EVEN
PUBLIC HZDfindFirst
HZDfindFirst PROC
UseParam
mov	ax, param2
call	PutStringRough
mov	ax, RoughSEG[2]
call	GetMemE
push	ds
mov	ds, ax
mov	dx, roughOFF+2
mov	cx, param1
xor	al, al
mov	ah, 4Eh
int	21h
pop	ds
jc	goterr09
mov	bx, param3
mov	es, EMSseg
mov	eax, es:[roughOFF+2+1Eh]
mov	[bx], eax
mov	eax, es:[roughOFF+2+1Eh+4]
mov	[bx+4], eax
mov	eax, es:[roughOFF+2+1Eh+8]
mov	[bx+8], eax
mov	al, [roughOFF+2+1Eh+12]
mov	[bx+12], eax
mov	al, es:[roughOFF+2+15h]
xor	ah, ah
mov	[bx+13], ax
mov	eax, es:[roughOFF+2+1Ah]
mov	[bx+14], eax
mov	ax, es:[roughOFF+2+16h]
mov	cx, ax
and	cx, 11111b
shl	cx, 1
mov	[bx+18], cx
mov	cx, ax
shr	cx, 5
and	cx, 111111b
mov	[bx+20], cx
shr	ax, 11
mov	[bx+22], ax
mov	ax, es:[roughOFF+2+18h]
mov	cx, ax
and	cx, 11111b
mov	[bx+24], cx
mov	cx, ax
shr	cx, 5
and	cx, 1111b
mov	[bx+26], cx
shr	ax, 9
add	ax, 1980
mov	[bx+28], ax
mov	al, es:[roughOFF+2+0]
add	al, 65
mov	ah, ':'
mov	[bx+30], ax

over56:
EndParam
retf	6

goterr09:
mov	LastError, errfindfile
jmp	over56
HZDfindFirst ENDP
















END
